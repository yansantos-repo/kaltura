<?php 
ob_start();

require_once('arquivos/scripts/base.php'); // configurações da base de dados utilizada no sistema, não presente neste repositório
require_once('integracao/KalturaClient.php'); //Integração com o Kaltura para verificar porcentagem do curso

mysql_select_db($database_base, $base);
$qy_consulta = sprintf("SELECT * FROM certificados WHERE ra = '$usuario_logado' ORDER BY nome ASC"); //RA = Usuário a ser filtrado no Kaltura
$consulta = mysql_query($qy_consulta, $base) or die('Erro em consulta, contate o administrador do sistema!<br>Error in query, contact the system administrator!');
$row_consulta = mysql_fetch_assoc($consulta);
$total_consulta = mysql_num_rows($consulta);

do {

  $curso_atual = utf8_encode($row_consulta['curso']);
  $verificar_curso = $row_consulta['curso'];
  mysql_select_db($database_base, $base);
  $qy_total_horas = "SELECT sum(duracao) as total_horas FROM cursos WHERE nome_curso = '$curso_atual'";
  $total_horas = mysql_query($qy_total_horas, $base) or die('Erro em consulta, contate o administrador do sistema!<br>Error in query, contact the system administrator!');
  $row_total_horas = mysql_fetch_assoc($total_horas);
  $total_total_horas = mysql_num_rows($total_horas);

  $total_curso = $row_total_horas['total_horas'] / 60;


  $carga_horaria = $row_consulta['carga_horaria'];
  $data_inicio = explode("-", $row_consulta['data_inicio']);
  $data_fim = explode("-", $row_consulta['data_fim']);

  //Integração				
  $config = new KalturaConfiguration();
  $config->serviceUrl = 'https://www.kaltura.com/';
  $client = new KalturaClient($config);

  $secret = 'Senha da API no Kaltura'; //Senha informada
  $userId = null;
  $type = KalturaSessionType::ADMIN;
  $partnerId = "Usuário da API no Kaltura"; //Usuário informado
  $expiry = null;
  $privileges = null;

  $reportType = KalturaReportType::SPEFICIC_USER_ENGAGEMENT; //Definição do tipo de consulta (User Engagement)
  $ks = $client->session->start($secret, null, $type, $partnerId, null, null);
  $client->setKs($ks);
  $reportInputFilter = new KalturaEndUserReportInputFilter(); // Classe responsável por criar o filtro

  $reportInputFilter->fromDay = $data_inicio[0] . $data_inicio[1] . $data_inicio[2]; //Intervalo em que será realizada a consulta
  $reportInputFilter->toDay = $data_fim[0] . $data_fim[1] . $data_fim[2]; //Intervalo em que será realizada a consulta

  $reportInputFilter->userIds = $ra; //usuário alvo do relatório
  $pager = new KalturaFilterPager();
  $pager->pageSize = 500;
  $order = null;
  $objectIds = null;
  $result = $client->report->getTable($reportType, $reportInputFilter, $pager, $order, $objectIds);
 
 /**
  * Abaixo o array de '$result' será tratado da seguinte forma:
  *
  * $coluna[0] = Nome do vídeo assistido pelo usuário ('entry_name' no array).
  * $coluna[3] = Tempo em minutos que o usuário gastou assistindo o vídeo ('sum_time_viewed' no array).
  * 
  * Com base nesses dois dados, o cálculo da porcentagem assistida é feito, partindo das informações prévias contidas no banco de dados (Tempo total que cada curso *tem, etc).
  *
  */

  //print_r($result); //Se der problema tire esse comentario par debugar		


  $total_linhas = 0;
  $porc_assistida = 0;
  $total_geral_cursos = 0;
  $total_videos = 0;
  $duracao2 = 0;
  foreach ($result as $itens) {

    if ($total_linhas == 1) {

      $linhas = explode(";", $itens);

      foreach ($linhas as $linha) {

        $colunas = explode(",", $linha);

        if ($colunas[0] != '') {


          $video_atual = $colunas[0];

          mysql_select_db($database_base, $base);
          $qy_cursos = "SELECT * FROM cursos WHERE nome_curso = '$curso_atual'"; // Faz uma consulta em um banco para buscar o curso que tem ligação com os vídeos retornados.
          $cursos = mysql_query($qy_cursos, $base) or die('Erro em consulta, contate o administrador do sistema!<br>Error in query, contact the system administrator!');
          $row_cursos = mysql_fetch_assoc($cursos);
          $total_cursos = mysql_num_rows($cursos);


          do {

            if (str_replace(" ", "", $video_atual) == str_replace(" ", "", $row_cursos['nome_video'])) {

              $total_geral_cursos = $total_cursos + $total_geral_cursos;

              $duracao = $row_cursos['duracao'] / 60;
			
			
              if ($colunas[3] > $duracao) {

                $porc_assistida_do_curso = 100; /// 100%
              } else {

                $porc_assistida_do_curso = ( $colunas[3] / $duracao ) * 100;
              };


              $porc_assistida = $porc_assistida + $porc_assistida_do_curso;

              $total_videos++;
            };
          } while ($row_cursos = mysql_fetch_assoc($cursos));
        };
      };
    };

    $total_linhas++;
  }

  if ($total_geral_cursos == 0) {
    $total_geral_cursos = 1;
  };
  if ($total_videos == 0) {
    $total_videos = 1;
  };

  
    $porc_assistida_final = $porc_assistida / ( $total_geral_cursos / $total_videos); //porcentagem final assistida pelo aluno
 


  if ($porc_assistida_final > 100) {

    $porc_assistida_final = 100;
  }
 } while ($row_consulta = mysql_fetch_assoc($consulta));